package com.schooling.activemq.producer;

import com.schooling.activemq.BaseJunit4Test;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * User：Tab.
 * Date：2018/4/26.
 */
public class TestProducer extends BaseJunit4Test {

    @Autowired
    private ActiveMQProducer activeMQProducer;

    @Test
    public void send() {
        for (int i = 1; i < 5; i++) {
            this.activeMQProducer.sendMessage("我是队列消息00" + i);
        }
        while (true) {}
    }
}