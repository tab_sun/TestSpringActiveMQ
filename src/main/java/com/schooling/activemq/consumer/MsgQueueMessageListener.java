package com.schooling.activemq.consumer;

import org.springframework.jms.listener.SessionAwareMessageListener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * Created with IntelliJ IDEA.
 * User：Tab.
 * Date：2018/4/26.
 * <p>
 * 消费者
 */
public class MsgQueueMessageListener implements SessionAwareMessageListener<Message> {

    @Override
    public void onMessage(Message message, Session session) throws JMSException {

        if (message instanceof TextMessage) {

            String msg = ((TextMessage) message).getText();

            System.out.println("============================================================");
            System.out.println("消费者收到的消息：" + msg);
            System.out.println("============================================================");

            try {
                if ("我是队列消息002".equals(msg)) {
                    throw new RuntimeException("故意抛出的异常");
                }
                // 只要被确认后  就会出队，接受失败没有确认成功，会在原队列里面
                message.acknowledge();
            } catch (Exception e) {
                // 此不可省略 重发信息使用
                session.recover();
            }
        }
    }
}
