package com.schooling.activemq;

import org.apache.activemq.transport.TransportListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * 消息传输监听
 * Created with IntelliJ IDEA.
 * User：Tab.
 * Date：2018/4/13.
 */
public class ActiveMQTransportListener implements TransportListener {

    private static Logger LOGGER = LoggerFactory.getLogger(ActiveMQTransportListener.class);

    /**
     * 对消息传输命令进行监控
     */
    @Override
    public void onCommand(Object command) {
        LOGGER.warn("onCommand -> 对消息传输命令进行监控..." + command.toString());
    }

    /**
     * 对监控到的异常进行触发
     */
    @Override
    public void onException(IOException error) {
        LOGGER.error("onException -> 对监控到的异常进行触发..." + error.toString());
    }

    /**
     * 当failover时触发
     */
    @Override
    public void transportInterupted() {
        LOGGER.warn("transportInterupted -> 消息服务器连接发生中断...");
        // 这里就可以状态进行标识了
    }

    @Override
    public void transportResumed() {
        LOGGER.info("transportResumed -> 消息服务器连接已恢复...");
        // 这里就可以进行状态标识了
    }
}
